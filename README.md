# grafana-influxdb-docker-compose

## Props

Note this repo was copied from https://github.com/jkehres/docker-compose-influxdb-grafana.
He did an amazing job setting this up, I just made a few changes to run it in my enviroinment.
If you're here from a google search, send him a coffee, not me!

## Hardware

This app is designed to run on a raspberry pi

## Intro

Multi-container Docker app built from the following services:

* [InfluxDB](https://github.com/influxdata/influxdb) - time series database
* [Chronograf](https://github.com/influxdata/chronograf) - admin UI for InfluxDB
* [Grafana](https://github.com/grafana/grafana) - visualization UI for InfluxDB

Useful for quickly setting up a monitoring stack for performance testing. Combine with [serverless-artillery](https://github.com/Nordstrom/serverless-artillery) and [artillery-plugin-influxdb](https://github.com/Nordstrom/artillery-plugin-influxdb) to create a performance testing environment in minutes.

## Quick Start

To start the app:

1. Install [docker-compose](https://docs.docker.com/compose/install/) on the docker host.
1. Clone this repo on the docker host.
1. Optionally, change default credentials or Grafana provisioning.
1. Run the following command from the root of the cloned repo:
```
docker-compose up -d
```

To stop the app:

1. Run the following command from the root of the cloned repo:
```
docker-compose down
```

## Ports

The services in the app run on the following ports:

| Host Port | Service |
| - | - |
| 3000 | Grafana |
| 8086 | InfluxDB |
| 8888 | Chronograf |

Note that Chronograf does not support username/password authentication. Anyone who can connect to the service has full admin access. This should be taken into account when forwarding ports as necessary.


## Volumes

The app creates the following named volumes (one for each service) so data is not lost when the app is stopped:

* influxdb
* chronograf
* grafana

## Users and Variables

The app creates two admin users - one for InfluxDB and one for Grafana. By default, the username and password of both accounts is `admin`. To override the default credentials, set the following environment variables before starting the app:

* `INFLUXDB_USERNAME`
* `INFLUXDB_PASSWORD`
* `GRAFANA_USERNAME`
* `GRAFANA_PASSWORD`

There are other variables needed for this app:

* `INFLUXDB_ORG` - Specif the [organization](https://docs.influxdata.com/influxdb/v2.2/organizations/)
* `INFLUXDB_BUCKET` - Specify the [influx bucket](https://docs.influxdata.com/influxdb/v2.2/organizations/buckets/)
* `INFLUXDB_RETENTION` - Set the [influx retention](https://github.com/docker-library/docs/blob/master/influxdb/README.md#using-this-image---influxdb-2x) (mapped to the DOCKER_INFLUXDB_INIT_RETENTION parameter)
* `INFLUXDB_ADMIN_TOKEN` - specify the [admin token](https://github.com/docker-library/docs/blob/master/influxdb/README.md#using-this-image---influxdb-2x).  Used to pass to Chronograf
* `CHRONOGRAF_PUBLIC_URL` - specify the [public url for chronograf](https://docs.influxdata.com/chronograf/v1.9/administration/config-options/)

## Data Sources

~~The app creates a Grafana data source called `InfluxDB` that's connected to the default IndfluxDB database (e.g. `db0`).~~

~~To provision additional data sources, see the Grafana [documentation](http://docs.grafana.org/administration/provisioning/#datasources) and add a config file to `./grafana-provisioning/datasources/` before starting the app.~~

## Dashboards

~~By default, the app does not create any Grafana dashboards. An example dashboard that's configured to work with [artillery-plugin-influxdb](https://github.com/Nordstrom/artillery-plugin-influxdb) is located at `./grafana-provisioning/dashboards/artillery.json.example`. To use this dashboard, rename it to `artillery.json`.~~

~~To provision additional dashboards, see the Grafana [documentation](http://docs.grafana.org/administration/provisioning/#dashboards) and add a config file to `./grafana-provisioning/dashboards/` before starting the app.~~
